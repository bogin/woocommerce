
<?php

$path = $_SERVER['DOCUMENT_ROOT'];
include_once $path . '/wp-config.php';
include_once $path . '/wp-load.php';
include_once $path . '/wp-includes/wp-db.php';
include_once $path . '/wp-includes/pluggable.php';
// include_once $path . '/woocommerce/wp-config.php';
// include_once $path . '/woocommerce/wp-load.php';
// include_once $path . '/woocommerce/wp-includes/wp-db.php';
// include_once $path . '/woocommerce/wp-includes/pluggable.php';
global $wpdb;

$table_name = $wpdb->prefix . "onePlusOne";
$sql = "CREATE TABLE $table_name (
  id int(9) NOT NULL,
  kindOfDeal varchar(255) NOT NULL,
  numOfDeals int(9) NOT NULL,
  price varchar(255) NOT NULL,
  timeStart datetime NOT NULL,
  name varchar(255) NOT NULL,
  description varchar(555) NOT NULL,
  imgSrc varchar(255) NOT NULL,
  shopName varchar(255) NOT NULL,
  PRIMARY KEY (id)
) ;";
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);
$kindOfDeal =  isset($_POST['kindOfDeal']) ? $_POST['kindOfDeal'] : '';
$numOfDeals = isset($_POST['numOfDeals']) ? $_POST['numOfDeals'] : '';
$id = isset($_POST['id']) ? $_POST['id'] : '';
$coupoId = isset($_POST['couponId']) ? $_POST['couponId'] : '';
$insertArray = array(
    'id' => $id,
    'kindOfDeal' => $kindOfDeal,
    'numOfDeals' => $numOfDeals,
    'price' => isset($_POST['price']) ? $_POST['price'] : '',
    'timeStart' => current_time( 'mysql' ),
    'name' => isset($_POST['name']) ? $_POST['name'] : '',
    'description' => isset($_POST['description']) ? $_POST['description'] : '',
    'imgSrc' => isset($_POST['imgSrc']) ? $_POST['imgSrc'] : '',
    'shopName' => isset($_POST['shopName']) ? $_POST['shopName'] : ''
    );

if(!is_null($insertArray['id'])) {
    $wpdb->insert($table_name,$insertArray,null );
}
createCoupon($kindOfDeal, $numOfDeals, $id,$coupoId);
// bogin_create_coupon1($coupoId);







function createCoupon($kindOfDeal, $numOfDeals, $id, $coupoId){

  $amount = $numOfDeals; // Amount
  $discount_type = 'percent_produc'; // Type: fixed_cart, percent, fixed_product, percent_product
  $coupon_code = substr( "abcdefghijklmnopqrstuvwxyz123456789", mt_rand(0, 50) , 1) .substr( md5( time() ), 1); // Code
  $coupon_code = substr( $coupon_code, 0,10); // create 10 letters coupon

  $coupon = array(
    'post_title' => '12',//$coupon_code,//$coupoId,
    'post_content' => '1+1coupon',
    'post_excerpt' => '1+1coupon',
    'post_status' => 'publish',
    'post_author' => 1,//$id,
    'post_type'		=> 'shop_coupon',
  );

  $new_coupon_id = wp_insert_post( $coupon );

  // Add meta
  update_post_meta( $new_coupon_id, 'discount_type', $discount_type );
  update_post_meta( $new_coupon_id, 'coupon_amount', '50');//$amount );
  update_post_meta( $new_coupon_id, 'individual_use', 'yes' );
  update_post_meta( $new_coupon_id, 'product_ids','');// $id );
  update_post_meta( $new_coupon_id, 'exclude_product_ids', '' );
  update_post_meta( $new_coupon_id, 'usage_limit', '1' );
  update_post_meta( $new_coupon_id, 'expiry_date', '2122-05-09' );
  update_post_meta( $new_coupon_id, 'apply_before_tax', 'no' );
  update_post_meta( $new_coupon_id, 'free_shipping', 'no' );
  update_post_meta( $new_coupon_id, 'exclude_sale_items', 'no' );
  update_post_meta( $new_coupon_id, 'product_categories', '' );
  update_post_meta( $new_coupon_id, 'exclude_product_categories', '' );
  update_post_meta( $new_coupon_id, 'minimum_amount', '' );
  update_post_meta( $new_coupon_id, 'customer_email', '' );

}
//
// add_filter ( 'woocommerce_get_shop_coupon_data', 'bogin_create_coupon', 10, 2  );
//
// function bogin_create_coupon( $data, $code ) {
//     // Check if the coupon has already been created in the database
//     global $wpdb;
//     $sql = $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type = 'shop_coupon' AND post_status = 'publish' ORDER BY post_date DESC LIMIT 1;", $code );
//     $coupon_id = $wpdb->get_var( $sql );
//     if ( empty( $coupon_id ) ) {
//         // Create a coupon with the properties you need
//         $data = array(
//             'discount_type'              => 'fixed_cart',
//             'coupon_amount'              => 100, // value
//             'individual_use'             => 'no',
//             'product_ids'                => array(26,27,28,29,30,31,32,33,34,35,36,37,38),
//             'exclude_product_ids'        => array(),
//             'usage_limit'                => '10000',
//             'usage_limit_per_user'       => '10000',
//             'limit_usage_to_x_items'     => '10000',
//             'usage_count'                => '',
//             'expiry_date'                => '2118-09-01', // YYYY-MM-DD
//             'free_shipping'              => 'no',
//             'product_categories'         => array(),
//             'exclude_product_categories' => array(),
//             'exclude_sale_items'         => 'no',
//             'minimum_amount'             => '0',
//             'maximum_amount'             => '',
//             'customer_email'             => array()
//         );
//         // Save the coupon in the database
//         $coupon = array(
//             'post_title' => $code,
//             'post_content' => '',
//             'post_status' => 'publish',
//             'post_author' => 1,
//             'post_type' => 'shop_coupon'
//         );
//         $new_coupon_id = wp_insert_post( $coupon );
//         // Write the $data values into postmeta table
//         foreach ($data as $key => $value) {
//             update_post_meta( $new_coupon_id, $key, $value );
//         }
//     }
//     return $data;
// }
// //
// function bogin_create_coupon1($coupon_code){
//     // Create the coupon
//     global $woocommerce;
//     $coupon = new WC_Coupon($coupon_code);
//
//     // Get the coupon discount amount (My coupon is a fixed value off)
//     $discount_total = $coupon->get_amount();
//
//     // Loop through products and apply the coupon discount
//     foreach($order->get_items() as $order_item){
//         $product_id = $order_item->get_product_id();
//
//         if($this->coupon_applies_to_product($coupon, $product_id)){
//             $total = $order_item->get_total();
//             $order_item->set_subtotal($total);
//             $order_item->set_total($total - $discount_total);
//             $order_item->save();
//         }
//     }
//     $order->save();
//
// }
// function coupon_applies_to_product($coupon, $product_id){
// $item = new WC_Order_Item_Coupon();
// $item->set_props(array('code' => $coupon_code, 'discount' => $discount_total, 'discount_tax' => 0));
// $order->add_item($item);
// $order->save();
// }
 ?>
