<?php
/*
Plugin Name: one plus one
Plugin URI:
Description:
Author: bogin
Author URI :
Version: 0.1
*/
if (!class_exists("OnePlusOne")) {
    class OnePlusOne {
        public $product;
        public $wpdb;

        public function addOpoAssets() {
            wp_register_style('opo-style', plugins_url().'/onePlusOne/css/boginstyle-index.css');
            wp_enqueue_style('opo-style');
            wp_register_script('opo-btn-script', plugins_url().'/onePlusOne/js/btn.js', ['jquery']);
            wp_enqueue_script('opo-btn-script');
        }

        public function echoOpoModal() {
            echo '<div class="opo-modal">'
                    .'<div class="opo-modal-content">'
                        .'<a href="javascript:void(0)" class="product-link">BUY ME BITCH</a>'
                        .'<span class="opo-close">X</span>'
                    .'</div>'
                .'</div>';
        }

        public function opoBtnMarkup($id) {
            return '<button class="opo-product-btn" data-opo-link="'.get_permalink($id).'" data-opo-id="'.$id.'">1+1</button>';
        }

        public function echoOpoBtn($id) {
            echo $this->opoBtnMarkup();
        }

        public function getShopName() {
            $query = "SELECT option_value FROM `wp_options` WHERE option_name = 'blogname'";
            $result = $this->wpdb->get_results($query);
            return get_object_vars($result[0])['option_value'];
        }

        public function addOpoBtnToCatalog($btn) {
            $id = (explode('data-product_id=', $btn));
            $id = explode(' ', $id[1]);
            $id = str_replace('"', '', $id[0]);
            $query = "SELECT * FROM `wp_onePlusOne` WHERE `id` = '$id'";
            $result = $this->wpdb->get_results($query);
            if(!empty($result)){
                $btn .= $this->opoBtnMarkup($id);
            }
            return $btn;
        }

        function __construct() {
            global $product, $wpdb;
            $this->product = $product;
            $this->wpdb = $wpdb;
            $this->addOpoAssets();
            // button - catalog
            add_filter('woocommerce_loop_add_to_cart_link', array($this, 'addOpoBtnToCatalog'));
            // button - single product
            add_action('woocommerce_after_add_to_cart_button', array($this, 'echoOpoBtn'));
            // modal - catalog
            add_action('woocommerce_after_shop_loop', array($this, 'echoOpoModal'));
            // modal - single product
            add_action('woocommerce_after_single_product_summary', array($this, 'echoOpoModal'));
        }
    }

    $opo = new OnePlusOne;
}


// add_filter ( 'woocommerce_get_shop_coupon_data', 'bogin_create_coupon', 10, 2  );

// this id the style for the dash board
add_action("admin_menu", "controllProduct_bogin");
function controllProduct_bogin(){
wp_enqueue_style( 'boginstyle-index',plugins_url('/boginstyle-index/boginstyle-index.css', __FILE__ ), '', '1.0', 'all' );
}

add_action('wp_dashboard_setup', 'one_plus_one_dashboard_widget_by_bogin');


//dashboard text
function sayHyToOwnnerByBogin(){
echo "to start using as' press configure";
}
function one_plus_one_dashboard_widget_by_bogin() {
wp_add_dashboard_widget('one_plus_one_controll_panel_by_bogin', 'One Plus One Controll Panel','sayHyToOwnnerByBogin', 'dashboard_handle_products_function_by_bogin');
}


//build the dash board products
function dashboard_handle_products_function_by_bogin() {
global $wpdb;
$temp = buildArraysProducts_bogin();
$productsInfoArray = $temp[0];
$shopName = $temp[1];

?>
</form>
<script>
jQuery(document).ready(function() {
jQuery(".sendForm").click(function(e) {
e.preventDefault();
var data = {},
inputs = jQuery(this).closest("form").find("input");

data['shopId'] = uniqId();
data['views'] = 0;
data['couponId'] = uniqId();
inputs.each(function() {
var $input = jQuery(this);
data[$input.attr("id")] = $input.val();
});

// saveToOnePlusOneDb(data);
saveToStore(data);
});
});



function saveToStore(data) {
jQuery.post("http://www.bogss.co.il/wp-content/plugins/onePlusOne/saveToOnePlusOneDb.php", data).success(function(response){
console.log(response);
}).fail(function(response){
console.log(response);
});
}



function uniqId() {
return Math.round(new Date().getTime() + (Math.random() * 100));
}
</script>
<div>
<?php
foreach ($productsInfoArray as $product) {  // $stoke = ($product[4]==null) ? 100 : $product[4];

?>
<div class='container'>
<img src="<?php echo "$product[3]" ?>" class='image' style='width: 400px;padding-top: 20px;padding-right: 14px;padding-bottom: 14px;'/>
<div class='middle'>

<form method="post" action="javascript:void(0)">
<input id="kindOfDeal" value="1" class='text' type='text' placeholder='סוג העסקה '/>
<input id="numOfDeals"  value="1"  class='text' type='text' placeholder='כמה עסקאות'/>
<input id="price"  class='text' value="1"  type='text' placeholder='סך הכל מחיר'/>
<input type="hidden" id="id" name="id" value='<?php echo "$product[0]"?>' />
<input type="hidden" id="name" name="name" value='<?php echo "$product[2]"?>' />
<input type="hidden" id="description" name="description" value='<?php echo "$product[1]"?>' />
<input type="hidden" id="imgSrc" name="imgSrc" value='<?php echo "$product[3]"?>' />
<input type="hidden" id="shopName" name="shopName" value='<?php echo "$shopName"?>' />

<button class="sendForm" type='button'>save</button>
</form>
</div>
</div>
<?php } ?>

</div>
<form method="post">

<?php
// saveToDb();
}

function saveToDb(){
global $wpdb;
$table_name = $wpdb->prefix . "onePlusOne";
$sql = "CREATE TABLE $table_name (
id_onePlusOne int(20) NOT NULL,
id int(9) NOT NULL,
kindOfDeal varchar(255) NOT NULL,
numOfDeals int(9) NOT NULL,
price varchar(255) NOT NULL,
timeStart datetime NOT NULL,
name varchar(255) NOT NULL,
description varchar(555) NOT NULL,
imgSrc varchar(255) NOT NULL,
shopName varchar(255) NOT NULL,
PRIMARY KEY  (id_onePlusOne)
) ;";
require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
dbDelta($sql);
}


function getShopName(){
global $wpdb;
$nameQuary = "SELECT option_value FROM `wp_options` WHERE option_name = 'blogname'";
$nameQuaryRes = $wpdb->get_results($nameQuary);
return get_object_vars($nameQuaryRes[0])['option_value'];

}
function buildArraysProducts_bogin(){
global $wpdb;
$shopName = getShopName();// get_object_vars($nameQuaryRes[0])['option_value'];
$quary = "SELECT * FROM wp_posts WHERE post_type = 'product'";
$quaryResults = $wpdb->get_results($quary);
$productsInfoArray = array();
$index = 0 ;

foreach($quaryResults as $itarator){
$productArray = array();
$productArray[0] = get_object_vars($itarator)['ID'];
$productArray[1] = get_object_vars($itarator)['post_content'];
$productArray[2] = get_object_vars($itarator)['post_title'] ;

$image = wp_get_attachment_image_src( get_post_thumbnail_id( $productArray[0] ), 'single-post-thumbnail' );
$productArray[3] = $image[0];
$product = wc_get_product($productArray[0]);
$productArray[4] = $product->get_stock_quantity();
$productsInfoArray[$index] = $productArray;
$index++;
}
return [$productsInfoArray,$shopName];
}


function dd($var){
echo "<pre>";
print_r($var);
echo "</pre>";
die;
}



?>
