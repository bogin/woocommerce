<?php
/*
Plugin Name: Authorize.net AIM - bogin WooCommerce Payment Gateway
Plugin URI:
Description: WooCommerce custom payment gateway integration .
Version: 1.0
*/

add_action( 'plugins_loaded', 'bogin_authorizenet_aim_init', 0 );
function bogin_authorizenet_aim_init() {
    //if condition use to do nothin while WooCommerce is not installed
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) return;
	include_once( 'bogin-authorize-woocommerce.php' );
	// class add it too WooCommerce
	add_filter( 'woocommerce_payment_gateways', 'bogin_add_authorizenet_aim_gateway' );
	function bogin_add_authorizenet_aim_gateway( $methods ) {
		$methods[] = 'bogin_AuthorizeNet_AIM';
		return $methods;
	}
}
// Add custom action links
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'bogin_authorizenet_aim_action_links' );
function bogin_authorizenet_aim_action_links( $links ) {
	$plugin_links = array(
		'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=checkout' ) . '">' . __( 'Settings', 'bogin-authorizenet-aim' ) . '</a>',
	);
	return array_merge( $plugin_links, $links );
}



?>
